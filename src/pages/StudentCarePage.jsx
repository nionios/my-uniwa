/*
  MIT License

  Copyright (c) 2022 Open Source  UOM

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  Made by Open Source UoM (https://opensource.uom.gr)

  Project members:
    -Apostolidis
    -Davios
    -Iosifidis
    -Konstantinidis
    -Mpakalis
    -Nasis
    -Omiliades
    -Patsouras
    -Fakidis

*/

import { useState, useEffect } from "react";
import { Box, Flex, useColorModeValue } from "@chakra-ui/react";
import { servicesData } from "../assets/services";
import ServicesCard from "../components/ServicesCard";

export default function StudentCarePage() {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <Box color={useColorModeValue("primary", "light")}>
      <Flex direction="column" align="center" w={"100%"}>
        <Box textAlign={"center"} w={{ sm: "90%", md: "90%", lg: "80%", "2xl": "60%", "3xl": "50%" }}>
          <ServicesCard
            srv={{
              url: "https://merimna.uniwa.gr/",
              title: "Διεύθυνση Φοιτητικής Μέριμνας",
              imgUrl: "https://mscmidwi.uniwa.gr/wp-content/uploads/sites/177/2019/04/logo.png",
            }}
          />
          <ServicesCard
            srv={{ url: "http://sitisi.uniwa.gr/", title: "Σίτιση", imgUrl: "http://sitisi.uniwa.gr/external/resources/assets/img/logos/Banner_logo.png?5" }}
          />
          <ServicesCard
            srv={{ url: "https://stegastiko.minedu.gov.gr/", title: "Στεγαστικό Επίδομα", imgUrl: "https://stegastiko.minedu.gov.gr/Content/images/home.png" }}
          />
          <ServicesCard
            srv={{
              url: "https://www.eopyy.gov.gr/",
              title: "Ευρωπαϊκή Κάρτα Ασφάλισης Ασθένειας (Ε.Κ.Α.Α.)",
              imgUrl: "https://www.eopyy.gov.gr/Template/images/logo_top.png",
            }}
          />
          <ServicesCard
            srv={{
              url: "http://www.iky.gr/",
              title: "Ίδρυμα Κρατικών Υποτροφιών",
              imgUrl: "https://www.iky.gr/templates/iky2014-main/images/iky-logo-web-2014.jpg",
            }}
          />
          <ServicesCard
            srv={{
              url: "http://www.minedu.gov.gr/aei-9/ypotrofies-klirodotimata-neo",
              title: "Υπουργείο Παιδείας – Υποτροφίες/Κληροδοτήματα",
              imgUrl: "https://www.minedu.gov.gr/images/banners/mainlogo.png",
            }}
          />
          {/* <ServicesCard srv={{ url: "", title: "", imgUrl: "" }} /> */}
        </Box>
      </Flex>
    </Box>
  );
}
