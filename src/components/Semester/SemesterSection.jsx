/*
  MIT License  
*/

import { Box, AccordionItem, AccordionButton, AccordionPanel, AccordionIcon, Accordion, Text, useColorModeValue } from "@chakra-ui/react";
import SemesterDay from "./SemesterDay";

function SemesterSection({ section }) {
  const dayClasses = [];
  if (Array.isArray(section.days)) {
    section.days.forEach((item) => dayClasses.push(item));
  }

  return (
    <Box marginBottom="1rem" borderRadius="10" overflow="hidden" border={"2px"} borderColor={useColorModeValue("primary", "primary")}>
      <AccordionItem border="none" w="100%">
        <AccordionButton
          display="flex"
          direction="row"
          alignItems="center"
          justifyContent="start"
          _hover={{ bg: "transparent" }}
          w="100%"
          h="100%"
          outline="none"
          textAlign="center"
          bgColor="transparent"
          color={useColorModeValue("primary", "white")}
          border="none"
          alt="profPic"
          overflow="hidden"
          gap={3}
        >
          <Text w="100%" display="flex" direction="row" alignItems="center" justifyContent="start" fontWeight="bold" fontSize={{ sm: 14, md: 16, lg: 18 }}>
            {section.name}
          </Text>
          <AccordionIcon />
        </AccordionButton>

        <AccordionPanel pb={5}>
          <Accordion allowToggle>
            {dayClasses.map((item) => {
              return <SemesterDay data={item} />;
            })}
          </Accordion>
        </AccordionPanel>
      </AccordionItem>
    </Box>
  );
}

export default SemesterSection;
