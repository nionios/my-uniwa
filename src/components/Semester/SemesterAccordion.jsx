/*
  MIT License  
*/

import { Box, AccordionItem, AccordionButton, AccordionPanel, AccordionIcon, Accordion, Text, useColorModeValue } from "@chakra-ui/react";
import SemesterSection from "./SemesterSection";

function SemesterAccordion({ semester }) {
  const semesterSections = [];
  if (Array.isArray(semester.sections)) {
    semester.sections.forEach((item) => semesterSections.push(item));
  }

  return (
    <Box marginBottom="1rem" borderRadius="0" overflow="hidden" borderBottom={"2px"} borderColor={useColorModeValue("primary", "primary")}>
      <AccordionItem border="none" w="100%">
        <AccordionButton
          display="flex"
          direction="row"
          alignItems="center"
          justifyContent="start"
          _hover={{ bg: "transparent" }}
          w="100%"
          h="100%"
          outline="none"
          textAlign="center"
          bgColor="transparent"
          color={useColorModeValue("primary", "white")}
          border="none"
          alt="profPic"
          overflow="hidden"
          gap={3}
        >
          <Text w="100%" display="flex" direction="row" alignItems="center" justifyContent="start" fontWeight="bold" fontSize={{ sm: 14, md: 16, lg: 18 }}>
            {semester.semester}
          </Text>
          <AccordionIcon />
        </AccordionButton>

        <AccordionPanel pb={5}>
          <Accordion allowToggle>
            {semesterSections.map((data) => {
              return <SemesterSection section={data} />;
            })}
          </Accordion>
        </AccordionPanel>
      </AccordionItem>
    </Box>
  );
}

export default SemesterAccordion;
