/*
  MIT License  
*/

import { Box, AccordionItem, AccordionButton, AccordionPanel, AccordionIcon, Accordion, Text, useColorModeValue } from "@chakra-ui/react";
import ExamsTimeslot from "./ExamsTimeslot";
import ExamsDay from "./ExamsDay";

function ExamsAccordion({ week }) {
  const weekDays = [];
  if (Array.isArray(week.days)) {
    week.days.forEach((item) => weekDays.push(item));
  }

  function EmptyDay(data, index) {
    return (
      <Box key={index} marginBottom="1rem" borderRadius="20" overflow="hidden" border="2px" borderColor={useColorModeValue("primary", "primary")}>
        <AccordionItem border="none" w="100%">
          <AccordionButton
            display="flex"
            direction="row"
            alignItems="center"
            justifyContent="start"
            _hover={{ bg: "transparent" }}
            w="100%"
            h="100%"
            outline="none"
            textAlign="center"
            bgColor="transparent"
            color={useColorModeValue("primary", "white")}
            border="none"
            alt="profPic"
            overflow="hidden"
            gap={3}
          >
            <Text w="100%" display="flex" direction="row" alignItems="center" justifyContent="start" fontWeight="bold" fontSize={{ sm: 14, md: 16, lg: 18 }}>
              {data.date} - {data.day}
            </Text>
          </AccordionButton>
        </AccordionItem>
      </Box>
    );
  }

  function Day(data, index) {
    return <ExamsDay key={index} day={data} />;
  }

  function RenderDay(data, index) {
    if (data.timeslots.length === 0) {
      return EmptyDay(data, index);
    }
    return Day(data, index);
  }

  return (
    <Box marginBottom="1rem" borderRadius="0" overflow="hidden" borderBottom={"2px"} borderColor={useColorModeValue("primary", "primary")}>
      <AccordionItem border="none" w="100%">
        <AccordionButton
          display="flex"
          direction="row"
          alignItems="center"
          justifyContent="start"
          _hover={{ bg: "transparent" }}
          w="100%"
          h="100%"
          outline="none"
          textAlign="center"
          bgColor="transparent"
          color={useColorModeValue("primary", "white")}
          border="none"
          alt="profPic"
          overflow="hidden"
          gap={3}
        >
          <Text w="100%" display="flex" direction="row" alignItems="center" justifyContent="start" fontWeight="bold" fontSize={{ sm: 14, md: 16, lg: 18 }}>
            {week.week}
          </Text>
          <AccordionIcon />
        </AccordionButton>

        <AccordionPanel pb={5}>
          <Accordion allowToggle>
            {weekDays.map((data, index) => {
              return RenderDay(data, index);
            })}
          </Accordion>
        </AccordionPanel>
      </AccordionItem>
    </Box>
  );
}

export default ExamsAccordion;
