/*
  MIT License

  Copyright (c) 2022 Open Source  UOM

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  Made by Open Source UoM (https://opensource.uom.gr)

  Project members:
    -Apostolidis
    -Davios
    -Iosifidis
    -Konstantinidis
    -Mpakalis
    -Nasis
    -Omiliades
    -Patsouras
    -Fakidis

*/

import {DEPARTMENTS} from "./DepNames";

export const schedulesData = new Map([
  [
    DEPARTMENTS[0],
    {
      semester:
        "https://www.uom.gr/assets/site/public/nodes/4635/14544-Orologio-Programma-Xeimerinoy-2022-2023.pdf",
      exam: "https://www.uom.gr/assets/site/public/nodes/4636/13840-Eksetastiki_Septembrios_2022.pdfs",
    },
  ],
  [
    DEPARTMENTS[1],
    {
      semester:
        "https://www.uom.gr/assets/site/public/nodes/4549/14545-2022_23_xeimerino2.pdf",
      exam: "https://www.uom.gr/assets/site/public/nodes/4550/13808-PROGRAMMA_EXETASEON_SEPTEMVRIOU_2022.pdf",
    },
  ],
  [
    DEPARTMENTS[2],
    {
      semester:
        "https://www.uom.gr/assets/site/public/nodes/5233/14233-orologioxeimerino20222023.pdf",
      exam: "https://www.uom.gr/assets/site/public/nodes/4865/13806-programmaeksetastikisseptemvriou2022-new.pdf",
    },
  ],
  [
    DEPARTMENTS[3],
    {
      semester:
        "https://docs.google.com/spreadsheets/d/1bUFgxecf6WgQcP3TFnQQwGYRoXDQYATg-WHl9lw1mS0/edit#gid=830689206",
      exam: "https://docs.google.com/spreadsheets/u/1/d/1PPcvhvLDDzJW3_SzqK3HAgx7PnJZdaijbRH3lN2O_mY/edit#gid=0",
    },
  ],
  [
    DEPARTMENTS[4],
    {
      semester:
        "https://www.uom.gr/assets/site/public/nodes/5180/14507-accfin-orologio-programma-xeimerino-2022-2023-20221006.pdf",
      exam: "https://www.uom.gr/assets/site/public/nodes/4975/13804-accfin-exetaseis-september-2022-20220722.pdf",
    },
  ],
  [
    DEPARTMENTS[5],
    {
      semester:
        "https://www.uom.gr/assets/site/public/nodes/1381/14383-OROLOGIO_2022-23.pdf",
      exam: "https://www.uom.gr/assets/site/public/nodes/1382/13823-Programma_Exetastikhs_Septembriou_2022.pdf",
    },
  ],
  [
    DEPARTMENTS[6],
    {
      semester:
        "https://www.uom.gr/assets/site/public/nodes/3653/14480-neoprogrammadidaskalias_xeimerinou.xlsx",
      exam: "https://www.uom.gr/assets/site/public/nodes/3654/13750-september_programmeexamslast.xls",
    },
  ],
  [
    DEPARTMENTS[7],
    {
      semester:
        "https://www.uom.gr/assets/site/public/nodes/12268/14532-2022-2023_xeimerino_ode_v1.pdf",
      exam: "https://www.uom.gr/assets/site/public/nodes/7471/13678-ODE_September_2022.pdf",
    },
  ],
]);
