/*
  MIT License

  Copyright (c) 2022 Open Source  UOM

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  Made by Open Source UoM (https://opensource.uom.gr)

  Project members:
    -Apostolidis
    -Davios
    -Iosifidis
    -Konstantinidis
    -Mpakalis
    -Nasis
    -Omiliades
    -Patsouras
    -Fakidis

*/

import {useEffect, useReducer} from "react";
import {Flex, Text, useColorModeValue} from "@chakra-ui/react";
import firstWeekData from "../assets/menus/FirstWeekMenu.json";
import secondWeekData from "../assets/menus/SecondWeekMenu.json";

import FoodMenuList from "../components/FoodMenuList";

const CURRENTLY_LUNCH = "gevma";
const CURRENTLY_DINNER = "deipno";
const CURRENTLY_NEXT_LUNCH = "gevma epomenhs";

// Get week number to change meal data depending on week
// from https://stackoverflow.com/questions/6117814/get-week-of-year-in-javascript-like-in-php
Date.prototype.getWeekNumber = function () {
  var d = new Date(
    Date.UTC(this.getFullYear(), this.getMonth(), this.getDate())
  );
  var dayNum = d.getUTCDay() || 7;
  d.setUTCDate(d.getUTCDate() + 4 - dayNum);
  var yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
  return Math.ceil(((d - yearStart) / 86400000 + 1) / 7);
};

export default function TodaysMenu() {
  const [state, dispatch] = useReducer(reducer, {});

  function reducer(state, action) {
    switch (action.type) {
      case CURRENTLY_LUNCH: {
        const temp = getTodaysRestaurantMenu(0, true);
        return {isLunch: true, isTomorrow: false, foodMenu: temp};
      }
      case CURRENTLY_DINNER: {
        const temp = getTodaysRestaurantMenu(0, false);
        return {isLunch: false, isTomorrow: false, foodMenu: temp};
      }
      case CURRENTLY_NEXT_LUNCH: {
        const temp = getTodaysRestaurantMenu(1, true);
        return {isLunch: true, isTomorrow: true, foodMenu: temp};
      }
    }
  }

  function getNextMeal(curr_date) {
    const now = curr_date;
    const dayNum = now.getDay();
    const isWeekDay = !(dayNum === 6 || dayNum === 0);

    const [endingLunchHour, endingLunchMinutes] = [
      isWeekDay ? 15 : 30,
      isWeekDay ? 30 : 30,
    ];

    const [endingDinnerHour, endingDinnerMinutes] = [20, 0];

    const lunchTime = new Date(
      now.getFullYear(),
      now.getMonth(),
      now.getDate(),
      endingLunchHour,
      endingLunchMinutes
    );

    const dinnerTime = new Date(
      now.getFullYear(),
      now.getMonth(),
      now.getDate(),
      endingDinnerHour,
      endingDinnerMinutes
    );

    if (curr_date.getTime() <= lunchTime.getTime()) {
      return CURRENTLY_LUNCH;
    }

    if (curr_date.getTime() <= dinnerTime.getTime()) {
      return CURRENTLY_DINNER;
    }
    return CURRENTLY_NEXT_LUNCH;
  }

  function checkTimeSetNextMeal() {
    const menuToDisplay = getNextMeal(new Date());
    dispatch({type: menuToDisplay});
  }

  useEffect(() => {
    checkTimeSetNextMeal();
  }, []);

  function getTodaysRestaurantMenu(offsetDays, forLunch) {
    const days = [
      "ΚΥΡΙΑΚΗ",
      "ΔΕΥΤΕΡΑ",
      "ΤΡΙΤΗ",
      "ΤΕΤΑΡΤΗ",
      "ΠΕΜΠΤΗ",
      "ΠΑΡΑΣΚΕΥΗ",
      "ΣΑΒΒΑΤΟ",
    ];
    const dayNum = new Date().getDay();
    // Variable for notifying user on whick week the menu is
    let menuWeek;
    // Variable for loading week menu data
    let data;
    // If week is odd or even pick different week for meal data
    if (new Date().getWeekNumber() % 2 == 1) {
      data = firstWeekData;
      menuWeek = "1η";
    } else {
      data = secondWeekData;
      menuWeek = "2η";
    }

    //prettier-ignore
    const dayName = days[(dayNum + offsetDays) % 7];
    const todaysTotalMenu = data
      .filter((dayMenu) => {
        return dayMenu.day === dayName;
      })
      .pop();
    if (forLunch) {
      return {
        mainDish: todaysTotalMenu.gevmaKirios,
        specialDish: todaysTotalMenu.gevmaEidiko,
        side: todaysTotalMenu.gevmaSinodeutika,
        dessert: todaysTotalMenu.gevmaEpidorpio,
        menuWeek: menuWeek,
      };
    }

    return {
      mainDish: todaysTotalMenu.deipnoKirios,
      specialDish: todaysTotalMenu.deipnoEidiko,
      side: todaysTotalMenu.deipnoSinodeutika,
      dessert: todaysTotalMenu.deipnoEpidorpio,
      menuWeek: menuWeek,
    };
  }

  return (
    <Flex
      flexDirection={"column"}
      bg={useColorModeValue("primary", "primary")}
      color={useColorModeValue("light", "light")}
      height="fit-content"
    >

      <Text
        fontWeight={"medium"}
        color={useColorModeValue("secondary", "secondary")}
        fontSize={{sm: 20, md: 22, lg: 24}}
      >
        {/*Need null check because the state and its children are populated
           after the function runs, which means top level children
            (foodMenu) can be accessed and are undefined, but
             foodMenu.menuWeek is undefined at that time. */}
        {state.foodMenu && state.foodMenu.menuWeek} Εβδομάδα μενού
      </Text>
      <Text
        fontWeight={"bold"}
        // marginBottom="1rem"
        fontSize={{sm: 22, md: 24, lg: 26}}
      >
        {state.isTomorrow ? "Αυριανό" : "Σημερινό"} Μενού:
      </Text>
      <Text fontWeight="bold" fontSize={{sm: 18, md: 20, lg: 22}} as="span">
        {state.isLunch ? "Γεύμα" : "Δείπνο"}:
      </Text>
      {<FoodMenuList {...state.foodMenu} />}{" "}
    </Flex>
  );
}
