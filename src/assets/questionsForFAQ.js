/*
  MIT License

  Copyright (c) 2022 Open Source  UOM

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  Made by Open Source UoM (https://opensource.uom.gr)

  Project members:
    -Apostolidis
    -Davios
    -Iosifidis
    -Konstantinidis
    -Mpakalis
    -Nasis
    -Omiliades
    -Patsouras
    -Fakidis

*/

const data = [
  {
    question: "Σε ποιους απευθύνεται η εφαρμογή;",
    panel:
      "Απευθύνεται σε όλους τους φοιτητές/φοιτήτριες του πανεπιστημίου καθώς και στο διδακτικό και διοικητικό προσωπικό.",
  },
  {
    question: "Τι μπορώ να βρω στην εφαρμογή;",
    panel:
      "Η εφαρμογή παρουσιάζει στο κινητό όλες τις υπηρεσίες του πανεπιστημίου καθώς και ό,τι χρειάζεται ένας φοιτητής/φοιτήτρια κατά τη διάρκεια της φοιτητικής ζωής.",
  },
  {
    question: "Από πού μπορώ να εγκαταστήσω την εφαρμογή;",
    panel:
      "Η εφαρμογή μπορεί να προστεθεί στην αρχική σας οθόνη. Επιλέξτε τον browser σας, πατήστε στο μενού δεξιά, και μετά επιλέξτε 'Add to Home Screen'. Θα γίνει αυτόματη εγκατάσταση της εφαρμογής, στη συσκευή σας. ΠΡΟΣΟΧΗ! Για χρήστες iPhone, το συγκεκριμένο λειτουργεί μόνο στο Safari. Διαφορετικά, κρατήστε την ιστοσελίδα στους σελιδοδείκτες σας.",
  },
  {
    question: "Η εφαρμογή δουλεύει offline;",
    panel:
      "Προς το παρόν η εφαρμογή χρειάζεται να είστε συνδεδεμένοι/ες στο διαδίκτυο για να μπορέσει να σας παρουσιάσει τα δεδομένα που έχετε αναζητήσει. Σε επόμενη έκδοση της εφαρμογής πιθανόν να μη χρειάζεται διαδίκτυο για τη λειτουργία της. ",
  },
  {
    question: "Η εφαρμογή είναι δωρεάν;",
    panel:
      "Η εφαρμογή διατίθεται δωρεάν για όλους τους φοιτητές/φοιτήτριες και το προσωπικό του πανεπιστημίου.",
  },
  {
    question: "Ποιοι κατασκεύασαν την εφαρμογή;",
    panel:
      "Η αρχική εφαρμογή κατασκευάστηκε από την Ομάδα Ανοικτού Λογισμικού του Πανεπιστημίου Μακεδονίας.\nΗ παρούσα έκδοση αναπτύχθηκε από την ομάδα iamUniWA του Πανεπιστημίου Δυτικής Αττικής.",
  },
  {
    question: "Πού κρατούνται τα δεδομένα μου;",
    panel:
      "Δεν αποθηκεύονται προσωπικά σας δεδομένα σε κάποιον διακομιστή του πανεπιστημίου. Αποθηκεύεται στη συσκευή σας η σχολή που θα επιλέξετε κατά την είσοδό σας στην εφαρμογή ώστε να σας παρουσιάζονται τα δεδομένα που αφορούν τη σχολή σας και μόνο.",
  },
];

export default data;
