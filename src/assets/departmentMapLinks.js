const departmentExamsSchedules = [
    {
        department: "Τμήμα Αγωγής και Φροντίδας στην Πρώιμη Παιδική Ηλικία",
        path: "https://campusplan.uniwa.gr/1"
    },
    {
        department: "Τμήμα Αρχειονομίας, Βιβλιοθηκονομίας και Συστημάτων Πληροφόρησης",
        path: "https://campusplan.uniwa.gr/1"
    },
    {
        department: "Τμήμα Βιοϊατρικών Επιστημών",
        path: "https://campusplan.uniwa.gr/1"
    },
    {
        department: "Τμήμα Γραφιστικής και Οπτικής Επικοινωνίας",
        path: "https://campusplan.uniwa.gr/1"
    },
    {
        department: "Τμήμα Δημόσιας και Κοινοτικής Υγείας",
        path: "https://campusplan.uniwa.gr/3"
    },
    {
        department: "Τμήμα Διοίκησης Επιχειρήσεων",
        path: "https://campusplan.uniwa.gr/2"
    },
    {
        department: "Τμήμα Διοίκησης Τουρισμού",
        path: "https://campusplan.uniwa.gr/1"
    },
    {
        department: "Τμήμα Επιστήμης και Τεχνολογίας Τροφίμων",
        path: "https://campusplan.uniwa.gr/1"
    },
    {
        department: "Τμήμα Επιστημών Οίνου, Αμπέλου και Ποτών",
        path: "https://campusplan.uniwa.gr/1"
    },
    {
        department: "Τμήμα Εργοθεραπείας",
        path: "https://campusplan.uniwa.gr/1"
    },
    {
        department: "Τμήμα Εσωτερικής Αρχιτεκτονικής",
        path: "https://campusplan.uniwa.gr/1"
    },
    {
        department: "Τμήμα Ηλεκτρολόγων και Ηλεκτρονικών Μηχανικών",
        path: "https://campusplan.uniwa.gr/2"
    },
    {
        department: "Τμήμα Κοινωνικής Εργασίας",
        path: "https://campusplan.uniwa.gr/2"
    },
    {
        department: "Τμήμα Λογιστικής και Χρηματοοικονομικής",
        path: "https://campusplan.uniwa.gr/2"
    },
    {
        department: "Τμήμα Μαιευτικής",
        path: "https://campusplan.uniwa.gr/1"
    },
    {
        department: "Τμήμα Μηχανικών Βιοϊατρικής",
        path: "https://campusplan.uniwa.gr/1"
    },
    {
        department: "Τμήμα Μηχανικών Βιομηχανικής Σχεδίασης και Παραγωγής",
        path: "https://campusplan.uniwa.gr/2"
    },
    {
        department: "Τμήμα Μηχανικών Πληροφορικής και Υπολογιστών",
        path: "https://campusplan.uniwa.gr/1"
    },
    {
        department: "Τμήμα Μηχανικών Τοπογραφίας και Γεωπληροφορικής",
        path: "https://campusplan.uniwa.gr/1"
    },
    {
        department: "Τμήμα Μηχανολόγων Μηχανικών",
        path: "https://campusplan.uniwa.gr/2"
    },
    {
        department: "Τμήμα Ναυπηγών Μηχανικών",
        path: "https://campusplan.uniwa.gr/1"
    },
    {
        department: "Τμήμα Νοσηλευτικής",
        path: "https://campusplan.uniwa.gr/1"
    },
    {
        department: "Τμήμα Πολιτικών Δημόσιας Υγείας",
        path: "https://campusplan.uniwa.gr/1"
    },
    {
        department: "Τμήμα Πολιτικών Μηχανικών",
        path: "https://campusplan.uniwa.gr/2"
    },
    {
        department: "Τμήμα Συντήρησης Αρχαιοτήτων και Έργων Τέχνης",
        path: "https://campusplan.uniwa.gr/1"
    },
    {
        department: "Τμήμα Φυσικοθεραπείας",
        path: "https://campusplan.uniwa.gr/1"
    },
    {
        department: "Τμήμα Φωτογραφίας και Οπτικοακουστικών Τεχνών",
        path: "https://campusplan.uniwa.gr/1"
    },
];

export default departmentExamsSchedules;
