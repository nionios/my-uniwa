const iamUniWAMembers = [
  {
    name: "Χρήστος Τρούσσας (Επιβλέπων)",
    socialMedia: "",
    email: "",
    img: "",
  },
  {
    name: "Κωνσταντίνος Κρυστάλλης (Μέλος)",
    socialMedia: "",
    email: "",
    img: "",
  },
  {
    name: "Διονύσιος Νικολόπουλος (Μέλος)",
    socialMedia: "",
    email: "",
    img: "",
  },
  {
    name: "Φλάβιο Ντολλάνι (Μέλος)",
    socialMedia: "",
    email: "",
    img: "",
  },
];

export default iamUniWAMembers;
