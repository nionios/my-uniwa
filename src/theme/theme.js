/*
  MIT License

  Copyright (c) 2022 Open Source  UOM

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  Made by Open Source UoM (https://opensource.uom.gr)

  Project members:
    -Apostolidis
    -Davios
    -Iosifidis
    -Konstantinidis
    -Mpakalis
    -Nasis
    -Omiliades
    -Patsouras
    -Fakidis

*/

import { extendTheme } from "@chakra-ui/react";

const config = {
  // The initial color mode is inherited from the OS, try to get it from the
  // localStorage also (for when the user has no cookies)
  //initialColorMode: localStorage.getItem('chakra-ui-color-mode') || 'system',
  initialColorMode: 'system',
  useSystemColorMode: true,
}

const theme = extendTheme({
  colors: {
    darkprimary: "#06273E",
    primary: "#094169",
    secondary: "#7FB6D5",
    lightBlue: "#DFE6EB",
    dark: "#1d2021",
    light: "#FEFEFE",
  },
  fonts: {
    heading: `'Comfortaa', sans-serif`,
    body: `'Comfortaa', sans-serif`,
  },
  fontSizes: {},
  breakpoints: {
    sm: "280px",
    md: "768px",
    lg: "960px",
    xl: "1200px",
    xxl: "1400px",
    "2xl": "1600px",
    "3xl": "1921px",
  },
  config
});

export default theme;
