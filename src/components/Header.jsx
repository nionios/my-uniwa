/*
  MIT License

  Copyright (c) 2022 Open Source  UOM

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  Made by Open Source UoM (https://opensource.uom.gr)

  Project members:
    -Apostolidis
    -Davios
    -Iosifidis
    -Konstantinidis
    -Mpakalis
    -Nasis
    -Omiliades
    -Patsouras
    -Fakidis

*/

import { useEffect, useState } from "react";
import { Outlet, useNavigate, useLocation } from "react-router-dom";
import iamUniWALogo from "../assets/iamUniWALogo.png";
import { isMobile, isTablet } from "react-device-detect";
import { Flex, Box, Heading, Image, useColorModeValue } from "@chakra-ui/react";
import MenuButton from "./MenuButton";
import { Categories } from "../assets/categories";

function RouteDictionary(route) {
  if (route === "/") return "Αρχική";
  else {
    for (let i = 0; i < Categories.length; i++) {
      if (Categories[i].route === route) return Categories[i].title;
    }
  }
}

export default function Header() {
  const navigate = useNavigate();
  const loc = useLocation();
  const [currentName, setCurrentName] = useState(RouteDictionary(loc.pathname));
  useEffect(() => {
    setCurrentName(RouteDictionary(`${loc.pathname}`));
  }, [loc]);

  const goToHomePage = () => {
    navigate("/");
  };

  const emergencyCall = () => {
    window.open("tel: 2105385144", "_self");
  };

  return (
    <>
      <Flex
        bgColor={useColorModeValue("light", "darkprimary")}
        position={{ sm: "relative", lg: "sticky" }}
        direction="row"
        w="100%"
        alignItems="center"
        zIndex={99}
        justifyContent="space-between"
        padding="1rem"
        top="0"
      >
        <Flex w={"fit-content"} direction={"row"} alignItems="center" justifyContent="space-between" gap={4}>
          <Flex alignItems="center" justifyContent="space-between" w={{ sm: "100%", lg: "fit-content" }}>
            <Image src={iamUniWALogo} onClick={goToHomePage} cursor="pointer" w={{ sm: "100px", xl: "150px" }} />
            <Box
              display={{ sm: "block", lg: "none" }}
              className={useColorModeValue("light-mode-svg", "dark-mode-svg")}
              marginLeft={{ sm: `${loc.pathname !== "/" ? "10px" : "0px"}` }}
            >
              {loc.pathname !== "/" ? (
                <Box cursor="pointer" onClick={goToHomePage} w={{ sm: "20px", lg: "20px", xl: "24px" }}>
                  <svg className="back-btn-svg" width="100%" viewBox="0 0 11 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M1 5.66666H10.0575" strokeLinecap="round" strokeLinejoin="round" />
                    <path d="M5.52881 0.999985L10.0575 5.66665L5.52881 10.3333" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </Box>
              ) : null}
            </Box>
          </Flex>
          <Flex className={useColorModeValue("light-mode-svg", "dark-mode-svg")} direction="column" alignItems="start" justifyContent="start" gap={1}>
            <Heading fontSize={{ sm: 32, lg: 34 }} color={useColorModeValue("black", "light")} fontWeight={500}>
              {currentName}
            </Heading>
            {loc.pathname !== "/" ? (
              <Box cursor="pointer" onClick={goToHomePage} w={{ sm: "20px", lg: "20px", xl: "24px" }} display={{ sm: "none", lg: "block" }}>
                <svg className="back-btn-svg" width="100%" viewBox="0 0 11 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M1 5.66666H10.0575" strokeLinecap="round" strokeLinejoin="round" />
                  <path d="M5.52881 0.999985L10.0575 5.66665L5.52881 10.3333" strokeLinecap="round" strokeLinejoin="round" />
                </svg>
              </Box>
            ) : null}
          </Flex>
        </Flex>
        <Box display={{ sm: "none", lg: "block" }}>
          <MenuButton />
        </Box>
      </Flex>
      {/* The navbar on mobile devices */}

      <Flex
        w="100%"
        paddingX="20px"
        zIndex={99}
        height="60px"
        alignItems="center"
        justifyContent="space-around"
        backgroundColor={useColorModeValue("lightBlue", "primary")}
        display={isMobile || isTablet ? "flex" : "none"}
        position="fixed"
        bottom="0"
      >
        <Box onClick={goToHomePage}>
          <svg className="home-svg" width="28" height="30.8" viewBox="0 0 20 22" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              d="M1 8L10 1L19 8V19C19 19.5304 18.7893 20.0391 18.4142 20.4142C18.0391 20.7893 17.5304 21 17 21H3C2.46957 21 1.96086 20.7893 1.58579 20.4142C1.21071 20.0391 1 19.5304 1 19V8Z"
              strokeLinecap="round"
              strokeLinejoin="round"
              stroke={useColorModeValue("#094169", "#eee")}
            />
            <path d="M7 21V11H13V21" strokeLinecap="round" strokeLinejoin="round" stroke={useColorModeValue("#094169", "#eee")} />
          </svg>
        </Box>
        <MenuButton />
        <Box onClick={emergencyCall}>
          <svg className="phone-svg" width="36" height="36" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"
              strokeLinecap="round"
              strokeLinejoin="round"
              stroke="red"
            />
            {/* <path d="M7 21V11H13V21" strokeLinecap="round" strokeLinejoin="round" stroke={useColorModeValue("#094169", "#eee")} /> */}
          </svg>
        </Box>
      </Flex>
      <Outlet />
    </>
  );
}
