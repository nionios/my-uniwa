/*
  MIT License

  Copyright (c) 2022 Open Source  UOM

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  Made by Open Source UoM (https://opensource.uom.gr)

  Project members:
    -Apostolidis
    -Davios
    -Iosifidis
    -Konstantinidis
    -Mpakalis
    -Nasis
    -Omiliades
    -Patsouras
    -Fakidis

*/

import {
  Flex,
  Box,
  Text,
  Button,
  Image,
  HStack,
  useColorModeValue,
} from "@chakra-ui/react";

import {
  TimeIcon,
  PhoneIcon,
  ExternalLinkIcon,
  ArrowForwardIcon,
} from "@chakra-ui/icons";

import LibraryCard from "../components/LibraryCard";
import libraryData from "../assets/libraryData";

export default function LibraryPage() {
  return (
    <Flex overflowX="hidden" flexDirection="column" alignItems="center">
      {libraryData.map((data, index) => (
        <LibraryCard key={index} data={data} />
      ))}
      <Button
        color={useColorModeValue("primary", "light")}
        variant="ghost"
        fontWeight="bold"
        fontSize={{ base: "lg", lg: "2xl" }}
        mt={{ base: "1rem", lg: "4rem" }}
        mb={"1rem"}
        rightIcon={
          <Box ml="0.5rem">
            <svg
              width="15px"
              viewBox="0 0 10 10"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M0.873535 9L8.91951 1"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={1.5}
                stroke={useColorModeValue("#094169", "#FEFEFE")}
              />
              <path
                d="M0.873535 1H8.91951V9"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={1.5}
                stroke={useColorModeValue("#094169", "#FEFEFE")}
              />
            </svg>
          </Box>
        }
        onClick={e => {
          window.open(
            "https://www.uniwa.gr/to-panepistimio/panepistimiakes-monades/vivliothikes/"
          );
        }}
        justifyContent="center"
      >
        Ιστοσελίδα Βιβλιοθήκης
      </Button>
    </Flex>
  );
}
