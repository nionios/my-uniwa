/*
  MIT License

  Copyright (c) 2022 Open Source  UOM

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  Made by Open Source UoM (https://opensource.uom.gr)

  Project members:
    -Apostolidis
    -Davios
    -Iosifidis
    -Konstantinidis
    -Mpakalis
    -Nasis
    -Omiliades
    -Patsouras
    -Fakidis

*/

const academicPersonnelData = [
  {
    "department": "Τμήμα Αγωγής και Φροντίδας στην Πρώιμη Παιδική Ηλικία",
    "pageUrl": "https://ecec.uniwa.gr/profiles/faculty/"
  },
  {
    "department": "Τμήμα Αρχειονομίας, Βιβλιοθηκονομίας και Συστημάτων Πληροφόρησης",
    "pageUrl": "https://alis.uniwa.gr/profiles/faculty"
  },
  {
    "department": "Τμήμα Βιοϊατρικών Επιστημών",
    "pageUrl": "https://bisc.uniwa.gr/profiles/faculty/"
  },
  {
    "department": "Τμήμα Γραφιστικής και Οπτικής Επικοινωνίας",
    "pageUrl": "https://gd.uniwa.gr/profiles/faculty/"
  },
  {
    "department": "Τμήμα Δημόσιας και Κοινοτικής Υγείας",
    "pageUrl": "https://pch.uniwa.gr/profiles/faculty/"
  },
  {
    "department": "Τμήμα Διοίκησης Επιχειρήσεων",
    "pageUrl": "http://www.ba.uniwa.gr/dep/"
  },
  {
    "department": "Τμήμα Διοίκησης Τουρισμού",
    "pageUrl": "https://tourism.uniwa.gr/profiles/faculty/"
  },
  {
    "department": "Τμήμα Επιστήμης και Τεχνολογίας Τροφίμων",
    "pageUrl": "https://fst.uniwa.gr/profiles/faculty/"
  },
  {
    "department": "Τμήμα Επιστημών Οίνου, Αμπέλου και Ποτών",
    "pageUrl": "https://wvbs.uniwa.gr/profiles/faculty/"
  },
  {
    "department": "Τμήμα Εργοθεραπείας",
    "pageUrl": "https://ot.uniwa.gr/profiles/faculty/"
  },
  {
    "department": "Τμήμα Εσωτερικής Αρχιτεκτονικής",
    "pageUrl": "https://ia.uniwa.gr/profiles/faculty/"
  },
  {
    "department": "Τμήμα Ηλεκτρολόγων και Ηλεκτρονικών Μηχανικών",
    "pageUrl": "https://eee.uniwa.gr/el/personnel-el"
  },
  {
    "department": "Τμήμα Κοινωνικής Εργασίας",
    "pageUrl": "https://sw.uniwa.gr/profiles/faculty/"
  },
  {
    "department": "Τμήμα Λογιστικής και Χρηματοοικονομικής",
    "pageUrl": "https://accfin.uniwa.gr/profiles/faculty/"
  },
  {
    "department": "Τμήμα Μαιευτικής",
    "pageUrl": "https://midw.uniwa.gr/profiles/meli-dep/"
  },
  {
    "department": "Τμήμα Μηχανικών Βιοϊατρικής",
    "pageUrl": "https://bme.uniwa.gr/profiles/faculty/"
  },
  {
    "department": "Τμήμα Μηχανικών Βιομηχανικής Σχεδίασης και Παραγωγής",
    "pageUrl": "https://idpe.uniwa.gr/el/academic-staff/teaching-staff"
  },
  {
    "department": "Τμήμα Μηχανικών Πληροφορικής και Υπολογιστών",
    "pageUrl": "http://www.ice.uniwa.gr/staff/faculty/"
  },
  {
    "department": "Τμήμα Μηχανικών Τοπογραφίας και Γεωπληροφορικής",
    "pageUrl": "https://geo.uniwa.gr/profiles/faculty/"
  },
  {
    "department": "Τμήμα Μηχανολόγων Μηχανικών",
    "pageUrl": "https://mech.uniwa.gr/profiles/faculty/"
  },
  {
    "department": "Τμήμα Ναυπηγών Μηχανικών",
    "pageUrl": "http://www.na.uniwa.gr/dep/"
  },
  {
    "department": "Τμήμα Νοσηλευτικής",
    "pageUrl": "https://nurs.uniwa.gr/profiles/faculty/"
  },
  {
    "department": "Τμήμα Πολιτικών Δημόσιας Υγείας",
    "pageUrl": "https://php.uniwa.gr/profiles/faculty/"
  },
  {
    "department": "Τμήμα Πολιτικών Μηχανικών",
    "pageUrl": "http://www.civ.uniwa.gr/profiles/faculty/"
  },
  {
    "department": "Τμήμα Συντήρησης Αρχαιοτήτων και Έργων Τέχνης",
    "pageUrl": "https://cons.uniwa.gr/profiles/faculty/"
  },
  {
    "department": "Τμήμα Φυσικοθεραπείας",
    "pageUrl": "http://www.phys.uniwa.gr/profiles/faculty-members/"
  },
  {
    "department": "Τμήμα Φωτογραφίας και Οπτικοακουστικών Τεχνών",
    "pageUrl": "https://phaa.uniwa.gr/profiles/faculty/"
  }
];

export default academicPersonnelData;
