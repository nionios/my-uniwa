/*
  MIT License
*/

import { useContext, useEffect } from "react";
import ExamsAccordion from "../components/Exams/ExamsAccordion";
import departmentExamsSchedules from "../assets/departmentExamsSchedules";
import { DepartmentContext } from "../contexts/departmentContext";
import { Accordion, Box, Flex, Heading, Text, useColorModeValue } from "@chakra-ui/react";

export default function SemesterSchedulePage() {
  const smFontSize = 12;
  const mdFontSize = 16;
  const lgFontSize = 20;

  const { depName } = useContext(DepartmentContext);

  const scheduleIndex = departmentExamsSchedules.findIndex((i) => i.department === depName);

  const path = departmentExamsSchedules[scheduleIndex].path;

  const examsSchedule = path ? require(`../assets/Schedules/${path}/ExamsSchedule.json`) : null;

  return (
    <Box color={useColorModeValue("primary", "white")}>
      {depName ? (
        <Flex direction="column" align="center">
          <Box textAlign={"center"} w={{ sm: "90%", md: "90%", lg: "80%", "2xl": "60%", "3xl": "50%" }}>
            {examsSchedule ? (
              <Accordion allowToggle>
                {examsSchedule.map((data, index) => {
                  return <ExamsAccordion key={index} week={data} />;
                })}
              </Accordion>
            ) : (
              <Text
                w="100%"
                style={{ whiteSpace: "pre-line" }}
                display="flex"
                direction="row"
                alignItems="flex-start"
                justifyContent="center"
                fontSize={{ sm: smFontSize, md: mdFontSize, lg: lgFontSize }}
              >
                Το πρόγραμμα για το {depName} δεν βρέθηκε.
              </Text>
            )}
          </Box>
        </Flex>
      ) : (
        <Heading textAlign="center" marginTop="50px">
          Παρακαλώ επιλέξτε τμήμα από τις ρυθμίσεις.
        </Heading>
      )}
    </Box>
  );
}
