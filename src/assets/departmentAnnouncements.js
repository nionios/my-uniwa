const departmentAnnouncements = [
  {
    department: "Τμήμα Αγωγής και Φροντίδας στην Πρώιμη Παιδική Ηλικία",
    pageUrl: "https://ecec.uniwa.gr/category/announcements/",
  },
  {
    department:
      "Τμήμα Αρχειονομίας, Βιβλιοθηκονομίας και Συστημάτων Πληροφόρησης",
    pageUrl: "https://alis.uniwa.gr/category/notices",
  },
  {
    department: "Τμήμα Βιοϊατρικών Επιστημών",
    pageUrl: "https://bisc.uniwa.gr/category/announcements/",
  },
  {
    department: "Τμήμα Γραφιστικής και Οπτικής Επικοινωνίας",
    pageUrl: "https://gd.uniwa.gr/category/announcements/",
  },
  {
    department: "Τμήμα Δημόσιας και Κοινοτικής Υγείας",
    pageUrl: "https://pch.uniwa.gr/category/announcements/",
  },
  {
    department: "Τμήμα Διοίκησης Επιχειρήσεων",
    pageUrl: "http://www.ba.uniwa.gr/category/announcements/",
  },
  {
    department: "Τμήμα Διοίκησης Τουρισμού",
    pageUrl: "https://tourism.uniwa.gr/category/announcements/",
  },
  {
    department: "Τμήμα Επιστήμης και Τεχνολογίας Τροφίμων",
    pageUrl: "https://fst.uniwa.gr/category/announcements/",
  },
  {
    department: "Τμήμα Επιστημών Οίνου, Αμπέλου και Ποτών",
    pageUrl: "https://wvbs.uniwa.gr/category/announcements/",
  },
  {
    department: "Τμήμα Εργοθεραπείας",
    pageUrl: "https://ot.uniwa.gr/category/announcements/",
  },
  {
    department: "Τμήμα Εσωτερικής Αρχιτεκτονικής",
    pageUrl: "https://ia.uniwa.gr/category/announcements/",
  },
  {
    department: "Τμήμα Ηλεκτρολόγων και Ηλεκτρονικών Μηχανικών",
    pageUrl: "https://eee.uniwa.gr/el/anakinoseis/anakoinoseis-grammateias",
  },
  {
    department: "Τμήμα Κοινωνικής Εργασίας",
    pageUrl: "https://sw.uniwa.gr/category/announcements/",
  },
  {
    department: "Τμήμα Λογιστικής και Χρηματοοικονομικής",
    pageUrl: "https://accfin.uniwa.gr/category/announcements/",
  },
  {
    department: "Τμήμα Μαιευτικής",
    pageUrl: "https://midw.uniwa.gr/category/announcements/",
  },
  {
    department: "Τμήμα Μηχανικών Βιοϊατρικής",
    pageUrl: "https://bme.uniwa.gr/category/announcements/",
  },
  {
    department: "Τμήμα Μηχανικών Βιομηχανικής Σχεδίασης και Παραγωγής",
    pageUrl: "https://idpe.uniwa.gr/el/news/dept-news",
  },
  {
    department: "Τμήμα Μηχανικών Πληροφορικής και Υπολογιστών",
    pageUrl: "http://www.ice.uniwa.gr/announcements-all/",
  },
  {
    department: "Τμήμα Μηχανικών Τοπογραφίας και Γεωπληροφορικής",
    pageUrl: "https://geo.uniwa.gr/category/announcements/",
  },
  {
    department: "Τμήμα Μηχανολόγων Μηχανικών",
    pageUrl: "https://mech.uniwa.gr/category/anakoinoseis-grammateias/",
  },
  {
    department: "Τμήμα Ναυπηγών Μηχανικών",
    pageUrl: "http://www.na.uniwa.gr/category/announcements/general_a/",
  },
  {
    department: "Τμήμα Νοσηλευτικής",
    pageUrl: "https://nurs.uniwa.gr/category/announcements/",
  },
  {
    department: "Τμήμα Πολιτικών Δημόσιας Υγείας",
    pageUrl: "https://php.uniwa.gr/category/announcements/",
  },
  {
    department: "Τμήμα Πολιτικών Μηχανικών",
    pageUrl: "http://www.civ.uniwa.gr/announcements/",
  },
  {
    department: "Τμήμα Συντήρησης Αρχαιοτήτων και Έργων Τέχνης",
    pageUrl: "https://cons.uniwa.gr/category/announcements/",
  },
  {
    department: "Τμήμα Φυσικοθεραπείας",
    pageUrl: "http://www.phys.uniwa.gr/category/announcements/",
  },
  {
    department: "Τμήμα Φωτογραφίας και Οπτικοακουστικών Τεχνών",
    pageUrl: "https://phaa.uniwa.gr/category/announcements/",
  },
];

export default departmentAnnouncements;
