import React from "react";
import {
  Flex,
  Box,
  Grid,
  GridItem,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";

import { TimeIcon, PhoneIcon } from "@chakra-ui/icons";

const LibraryCard = ({ data }) => {
  return (
    <>
      <Text
        mt="2rem"
        mb="1rem"
        w="100%"
        display="flex"
        direction="row"
        justifyContent="center"
        fontWeight="bold"
        fontSize={{ sm: 20, md: 24, lg: 26 }}
      >
        {data.name}
      </Text>
      {/* Wrapper container */}
      <Flex
        textAlign="center"
        flexDirection={{ base: "column", lg: "row" }}
        columnGap={"3rem"}
        alignItems="center"
        justifyContent={"center"}
        width={{ sm: "100%", lg: "fit-content" }}
        paddingX="16px"
      >
        {/* Ωράριο */}
        <Box
          border="2px"
          borderRadius="1rem"
          bg={useColorModeValue("primary", "secondary")}
          borderColor={useColorModeValue("primary", "secondary")}
          marginBottom={{ base: "1rem", lg: "0" }}
          marginTop="1rem"
          px="1rem"
          py="1rem"
          width={{ base: "100%", lg: "60%" }}
          display={"flex"}
          justifyContent={"center"}
          alignItems={"center"}
          height={{ lg: "22.5vh" }}
        >
          <Flex
            flexDirection={"column"}
            alignItems={"center"}
            h="fit-content"
            w="100%"
            fontSize={{ base: "md", lg: "xl" }}
            color={useColorModeValue("light", "black")}
          >
            <Flex flexDirection={"row"} alignItems="center">
              <TimeIcon w={30} h={30} />
              <Text
                fontSize={{ base: "lg", lg: "2xl" }}
                ml="1rem"
                as="span"
                fontWeight={"bold"}
              >
                Ωράριο
              </Text>
            </Flex>
            <Flex mt="1rem" flexDirection={"row"} alignItems="center">
              <Box mx="0.5rem">
                <Text as="span" fontWeight={"bold"}>
                  Δευ-Πέμ
                </Text>
                <br />
                {data.hours.monday_thursday}
              </Box>
              <Box mx="1rem">
                <Text as="span" fontWeight={"bold"}>
                  Παρασκευή
                </Text>
                <br />
                {data.hours.friday}
              </Box>
              <Box mx="1rem">
                <Text as="span" fontWeight={"bold"}>
                  Σάβ-Κυρ
                </Text>
                <br />
                {data.hours.weekend}
              </Box>
            </Flex>
          </Flex>
        </Box>
        {/* Επικοινωνία  */}
        <Box
          border="2px"
          borderRadius="1rem"
          borderColor={useColorModeValue("primary", "secondary")}
          bg={useColorModeValue("primary", "secondary")}
          marginBottom={{ base: "1rem", lg: "0" }}
          mt="1rem"
          display={"flex"}
          justifyContent={"center"}
          alignItems={"center"}
          py="1rem"
          px="1rem"
          width={{ lg: "40%", base: "100%" }}
          height={{ lg: "22.5vh" }}
        >
          <Flex
            mx="1rem"
            flexDirection={"column"}
            alignItems="center"
            rowGap={"0.75rem"}
            justifyContent={"center"}
            columnGap={"1rem"}
            color={useColorModeValue("light", "black")}
            w="100%"
            fontSize={{ base: "md", lg: "2xl" }}
          >
            <Flex flexDirection={"row"} alignItems="center">
              <PhoneIcon w={27} h={27} />
              <Text
                fontSize={{ base: "lg", lg: "2xl" }}
                ml="1rem"
                as="span"
                fontWeight={"bold"}
              >
                Επικοινωνία
              </Text>
            </Flex>

            <Flex
              flexDirection={"row"}
              alignItems="center"
              justifyContent="center"
              w="100%"
              gap="2rem"
            >
              <Grid templateColumns="repeat(2, 1fr)" columnGap={8} rowGap={2}>
                {data.phone.map((number, index) => (
                  <Text key={index}>{number}</Text>
                ))}
              </Grid>
            </Flex>
          </Flex>
        </Box>
      </Flex>
    </>
  );
};

export default LibraryCard;
