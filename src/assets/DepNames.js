/*
  MIT License

  Copyright (c) 2022 Open Source  UOM

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  Made by Open Source UoM (https://opensource.uom.gr)

  Project members:
    -Apostolidis
    -Davios
    -Iosifidis
    -Konstantinidis
    -Mpakalis
    -Nasis
    -Omiliades
    -Patsouras
    -Fakidis

*/

export const DEPARTMENTS = [
  "Τμήμα Αγωγής και Φροντίδας στην Πρώιμη Παιδική Ηλικία",
  "Τμήμα Αρχειονομίας, Βιβλιοθηκονομίας και Συστημάτων Πληροφόρησης",
  "Τμήμα Βιοϊατρικών Επιστημών",
  "Τμήμα Γραφιστικής και Οπτικής Επικοινωνίας",
  "Τμήμα Δημόσιας και Κοινοτικής Υγείας",
  "Τμήμα Διοίκησης Επιχειρήσεων",
  "Τμήμα Διοίκησης Τουρισμού",
  "Τμήμα Επιστήμης και Τεχνολογίας Τροφίμων",
  "Τμήμα Επιστημών Οίνου, Αμπέλου και Ποτών",
  "Τμήμα Εργοθεραπείας",
  "Τμήμα Εσωτερικής Αρχιτεκτονικής",
  "Τμήμα Ηλεκτρολόγων και Ηλεκτρονικών Μηχανικών",
  "Τμήμα Κοινωνικής Εργασίας",
  "Τμήμα Λογιστικής και Χρηματοοικονομικής",
  "Τμήμα Μαιευτικής",
  "Τμήμα Μηχανικών Βιοϊατρικής",
  "Τμήμα Μηχανικών Βιομηχανικής Σχεδίασης και Παραγωγής",
  "Τμήμα Μηχανικών Πληροφορικής και Υπολογιστών",
  "Τμήμα Μηχανικών Τοπογραφίας και Γεωπληροφορικής",
  "Τμήμα Μηχανολόγων Μηχανικών",
  "Τμήμα Ναυπηγών Μηχανικών",
  "Τμήμα Νοσηλευτικής",
  "Τμήμα Πολιτικών Δημόσιας Υγείας",
  "Τμήμα Πολιτικών Μηχανικών",
  "Τμήμα Συντήρησης Αρχαιοτήτων και Έργων Τέχνης",
  "Τμήμα Φυσικοθεραπείας",
  "Τμήμα Φωτογραφίας και Οπτικοακουστικών Τεχνών",
];
