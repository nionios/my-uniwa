/*
  MIT License
*/

import { useContext, useEffect } from "react";
import SemesterAccordion from "../components/Semester/SemesterAccordion";
import departmentSemesterSchedules from "../assets/departmentSemesterSchedules";
import { DepartmentContext } from "../contexts/departmentContext";
import { Accordion, Box, Flex, Heading, Text, useColorModeValue } from "@chakra-ui/react";

export default function SemesterSchedulePage({ examsProp, semesterProp }) {
  const smFontSize = 12;
  const mdFontSize = 16;
  const lgFontSize = 20;

  const { depName } = useContext(DepartmentContext);

  const scheduleIndex = departmentSemesterSchedules.findIndex((i) => i.department === depName);

  const path = departmentSemesterSchedules[scheduleIndex].path;

  const semesterSchedule = path ? require(`../assets/Schedules/${path}/SemesterSchedule.json`) : null;

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <Box color={useColorModeValue("primary", "white")}>
      {depName ? (
        <Flex direction="column" align="center">
          <Box textAlign={"center"} w={{ sm: "90%", md: "90%", lg: "80%", "2xl": "60%", "3xl": "50%" }}>
            {semesterSchedule ? (
              <Accordion allowToggle>
                {semesterSchedule.map((data) => {
                  return <SemesterAccordion semester={data} />;
                })}
              </Accordion>
            ) : (
              <Text
                w="100%"
                style={{ whiteSpace: "pre-line" }}
                display="flex"
                direction="row"
                alignItems="flex-start"
                justifyContent="center"
                fontSize={{ sm: smFontSize, md: mdFontSize, lg: lgFontSize }}
              >
                Το πρόγραμμα για το {depName} δεν βρέθηκε.
              </Text>
            )}
          </Box>
        </Flex>
      ) : (
        <Heading textAlign="center" marginTop="50px">
          Παρακαλώ επιλέξτε τμήμα από τις ρυθμίσεις.
        </Heading>
      )}
    </Box>
  );
}
