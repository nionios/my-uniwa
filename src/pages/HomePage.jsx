/*
  MIT License

  Copyright (c) 2022 Open Source  UOM

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  Made by Open Source UoM (https://opensource.uom.gr)

  Project members:
    -Apostolidis
    -Davios
    -Iosifidis
    -Konstantinidis
    -Mpakalis
    -Nasis
    -Omiliades
    -Patsouras
    -Fakidis

*/

import { useState, useEffect, useContext } from "react";
import { isDesktop } from "react-device-detect";
import { Categories } from "../assets/categories";
import academicPersonnelPages from "../assets/academicPersonnelPages";
import departmentMapLinks from "../assets/departmentMapLinks";
import MenuBox from "../components/MenuBox";
import { Flex, Grid, Heading } from "@chakra-ui/react";
import departmentAnnouncements from "../assets/departmentAnnouncements";
import { motion } from "framer-motion";
import Search from "../components/Search";
import { DepartmentContext } from "../contexts/departmentContext";

const stagger = {
  inView: {
    transition: {
      staggerChildren: 0.1,
    },
  },
};

export default function HomePage() {
  // Get the current department name set from settings
  const { depName } = useContext(DepartmentContext);
  // Get the page that lists the academic personel for specific department
  const academicPersonnelSourceIndex = academicPersonnelPages.findIndex((i) => i.department === depName);
  // Replace the link on the categories json with the appropriate one each time
  const academicPersonnelIndex = Categories.findIndex((i) => i.title === "Ακαδημαϊκό Προσωπικό");

  const mapSourceIndex = departmentMapLinks.findIndex((i) => i.department === depName);

  const mapIndex = Categories.findIndex((i) => i.title === "Χάρτης");

  const announcementsSourceIndex = departmentAnnouncements.findIndex((i) => i.department === depName);

  const announcementsIndex = Categories.findIndex((i) => i.title === "Ανακοινώσεις");

  if (academicPersonnelIndex !== -1 && academicPersonnelSourceIndex !== -1) {
    Categories[academicPersonnelIndex].route = academicPersonnelPages[academicPersonnelSourceIndex].pageUrl;
  }

  if (mapIndex !== -1 && mapSourceIndex !== -1) {
    Categories[mapIndex].route = departmentMapLinks[mapSourceIndex].path;
  }

  if (announcementsIndex !== -1 && announcementsSourceIndex !== -1) {
    Categories[announcementsIndex].route = departmentAnnouncements[announcementsSourceIndex].pageUrl;
  }

  const [categoriesListForSearch, setCategoriesListForSearch] = useState(Categories);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return (
    <Grid
      id="main-grid"
      alignItems="center"
      justifyContent="center"
      gap="3rem"
      h={{ sm: "100%" }}
      px={!isDesktop ? "1rem" : "3rem"}
      py={!isDesktop ? "0.5rem" : "0"}
      overflow={"auto"}
    >
      <Grid
        as={motion.section}
        initial="initial"
        animate="inView"
        variants={stagger}
        className="home-grid"
        marginTop="20px"
        gap={{ sm: 2, md: 3, lg: 4, xl: 5, "2xl": 6, "3xl": 7 }}
        templateColumns={{
          sm: "repeat(3, 1fr)",
          md: "repeat(3, minmax(0, 1fr))",
          xl: "repeat(5, calc((var(--available-width) - 5rem) / 5))",
          xxl: "repeat(5, calc((var(--available-width) / 1.5) / 5))",
          "2xl": "repeat(5, calc((var(--available-width) / 1.75) / 5))",
          "3xl": "repeat(5, calc((2*var(--available-width)/3) / 5))",
        }}
      >
        {categoriesListForSearch.length === 0 ? (
          <Heading gridColumnStart={1} gridColumnEnd={3} fontSize={{ sm: 11.95, md: 16, lg: 26, xl: 32 }} w="100%" fontWeight={500}>
            Η αναζήτηση δεν επέστρεψε αποτελέσματα.
          </Heading>
        ) : null}
        {categoriesListForSearch.map((category) => (
          <MenuBox category={category} key={category.title} />
        ))}
      </Grid>
    </Grid>
  );
}
