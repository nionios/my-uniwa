/*
  MIT License  
*/

import { Box, AccordionItem, AccordionButton, AccordionPanel, AccordionIcon, Text, useColorModeValue } from "@chakra-ui/react";
import ExamsTimeslot from "./ExamsTimeslot";

function ExamsDay({ day }) {
  const dayTimeslots = [];
  if (Array.isArray(day.timeslots)) {
    day.timeslots.forEach((item) => dayTimeslots.push(item));
  }

  return (
    <Box marginBottom="1rem" borderRadius="20" overflow="hidden" border="2px" borderColor={useColorModeValue("primary", "primary")}>
      <AccordionItem border="none" w="100%">
        <AccordionButton
          display="flex"
          direction="row"
          alignItems="center"
          justifyContent="start"
          _hover={{ bg: "transparent" }}
          w="100%"
          h="100%"
          outline="none"
          textAlign="center"
          bgColor="transparent"
          color={useColorModeValue("primary", "white")}
          border="none"
          alt="profPic"
          overflow="hidden"
          gap={3}
        >
          <Text w="100%" display="flex" direction="row" alignItems="center" justifyContent="start" fontWeight="bold" fontSize={{ sm: 14, md: 16, lg: 18 }}>
            {day.date} - {day.day}
          </Text>
          <AccordionIcon />
        </AccordionButton>

        <AccordionPanel pb={5}>
          <Box borderBottom={"2px"} mb={"0.5rem"} borderColor={useColorModeValue("primary", "primary")}>
            <ExamsTimeslot timeslot={{ timeslot: "Ώρα", class: "Μάθημα", semester: "Εξάμηνο" }} />
          </Box>
          {dayTimeslots.map((data, index) => {
            return <ExamsTimeslot key={index} timeslot={data} />;
          })}
        </AccordionPanel>
      </AccordionItem>
    </Box>
  );
}

export default ExamsDay;
