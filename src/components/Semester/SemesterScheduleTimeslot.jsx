/*
  MIT License
*/

import { Text, Grid, useColorModeValue } from "@chakra-ui/react";

function SemesterScheduleTimeslot({ timeslot }) {
  const smFontSize = 7.5;
  const mdFontSize = 12;
  const lgFontSize = 16;

  return (
    <Grid borderBottom={"1px"} templateColumns={"1fr 6fr 3fr 2fr"} borderColor={useColorModeValue("primary", "primary")}>
      <Text
        w="100%"
        style={{ whiteSpace: "pre-line" }}
        display="flex"
        direction="row"
        alignItems="flex-start"
        justifyContent="center"
        color={useColorModeValue("primary", "white")}
        fontSize={{ sm: smFontSize, md: mdFontSize, lg: lgFontSize }}
      >
        {timeslot.timeslot}
      </Text>
      <Text
        w="100%"
        style={{ whiteSpace: "pre-line" }}
        display="flex"
        direction="row"
        alignItems="flex-start"
        justifyContent="center"
        color={useColorModeValue("primary", "white")}
        fontSize={{ sm: smFontSize, md: mdFontSize, lg: lgFontSize }}
      >
        {timeslot.class}
      </Text>
      <Text
        w="100%"
        style={{ whiteSpace: "pre-line" }}
        display="flex"
        direction="row"
        alignItems="flex-start"
        justifyContent="center"
        color={useColorModeValue("primary", "white")}
        fontSize={{ sm: smFontSize, md: mdFontSize, lg: lgFontSize }}
      >
        {timeslot.professor}
      </Text>
      <Text
        w="100%"
        style={{ whiteSpace: "pre-line" }}
        display="flex"
        direction="row"
        alignItems="flex-start"
        justifyContent="center"
        color={useColorModeValue("primary", "white")}
        fontSize={{ sm: smFontSize, md: mdFontSize, lg: lgFontSize }}
      >
        {timeslot.room}
      </Text>
    </Grid>
  );
}

export default SemesterScheduleTimeslot;
