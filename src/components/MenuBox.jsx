/*
  MIT License

  Copyright (c) 2022 Open Source  UOM

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  Made by Open Source UoM (https://opensource.uom.gr)

  Project members:
    -Apostolidis
    -Davios
    -Iosifidis
    -Konstantinidis
    -Mpakalis
    -Nasis
    -Omiliades
    -Patsouras
    -Fakidis

*/

import { useContext, useEffect } from "react";
import { Box, useColorModeValue, Heading, GridItem, Flex, useToast } from "@chakra-ui/react";
import { ExternalLinkIcon } from "@chakra-ui/icons";
import { useNavigate } from "react-router-dom";
import { DepartmentContext } from "../contexts/departmentContext";
import { DEPARTMENTS } from "../assets/DepNames";
import { motion } from "framer-motion";

const fadeIn = {
  initial: {
    opacity: 0,
  },
  inView: {
    opacity: 1,
    transition: {
      duration: 0.45,
      ease: "easeIn",
    },
  },
};

export default function MenuBox({ category }) {
  const { title, iconSVG, route, span, isExternal, requireSelection } = category;
  const { depName } = useContext(DepartmentContext);
  const toast = useToast();
  let condition = requireSelection && !DEPARTMENTS.includes(depName);

  const navigate = useNavigate();

  const handleNavigation = () => {
    isExternal
      ? // ? requireSelection
        //   ? window.location.replace(route, "_blank")
        window.open(route, "_blank")
      : navigate(route);
  };

  const handleSelection = () => {
    const id = "1";
    if (requireSelection && !depName) {
      if (!toast.isActive(id)) {
        toast({
          id,
          title: "Δεν έχει επιλεγεί τμήμα",
          description: "Παρακαλώ επιλέξτε τμήμα από τις ρυθμίσεις",
          status: "error",
          duration: 5000,
          isClosable: true,
          position: "bottom",
        });
      }
    } else {
      handleNavigation();
    }
  };

  return (
    <GridItem
      as={motion.div}
      variants={fadeIn}
      viewport={{ once: true }}
      cursor="pointer"
      onClick={handleSelection}
      colSpan={span}
      role={"group"}
      border="2px"
      display="flex"
      flexDirection="column"
      alignItems="center"
      justifyContent="space-between"
      transition=".5s"
      backgroundColor={useColorModeValue("primary", "transparent")}
      borderColor={useColorModeValue("primary", "light")}
      _hover={{
        bg: useColorModeValue("secondary", "primary"),
        borderColor: useColorModeValue("secondary", "primary"),
        transform: "scale(1.05)"
      }}
      _focus={{
        bg: useColorModeValue("secondary", "primary"),
        borderColor: useColorModeValue("secondary", "primary"),
      }}
      className={`menu-box-${span} ${condition ? "disabled" : ""}`}
      rounded="0.75rem"
      p={{ sm: 2, md: 4, lg: 6 }}
    >
      <Flex w="100%"
            flexDirection="row"
            alignItems="center"
            justifyContent="space-between">
        <Box
          minW={{ sm: "12px", md: "32px", "2xl": "52px" }}
          maxW={{ sm: "12px", md: "32px", "2xl": "52px" }}
          className={`svg-container ${useColorModeValue("light-mode-svg", "dark-mode-svg")}`}
        >
          {iconSVG}
        </Box>
        <Box
          minW={{ sm: "8px", md: "28px", "2xl": "32px" }}
          maxW={{ sm: "12px", md: "28px", "2xl": "32px" }}
          className={`svg-container ${useColorModeValue("light-mode-svg", "dark-mode-svg")}`}
        >
          {isExternal ? (
            <svg className="stroke-svg"
                 width="100%"
                 viewBox="0 0 10 10"
                 fill="none"
                 xmlns="http://www.w3.org/2000/svg">
              <path d="M0.873535 9L8.91951 1" strokeLinecap="round"
                    strokeLinejoin="round"/>
              <path d="M0.873535 1H8.91951V9" strokeLinecap="round"
                    strokeLinejoin="round"/>
            </svg>
          ) : (
            <svg className="stroke-svg"
                 width="120%"
                 viewBox="0 0 11 11"
                 fill="none"
                 xmlns="http://www.w3.org/2000/svg">
              <path d="M1 5.66666H10.0575" strokeLinecap="round"
                    strokeLinejoin="round"/>
              <path d="M5.52881 0.999985L10.0575 5.66665L5.52881 10.3333"
                    strokeLinecap="round" strokeLinejoin="round"/>
            </svg>
          )}
        </Box>
      </Flex>
      <Heading fontSize={{
        sm: "min(3vw, 12px)",
        md: 14, lg: 16, xl: 18, xxl: 22,
        "2xl": "min(1em, 24px)",
        "3xl": "1.5vw" }}
               color="light" w="100%" fontWeight={500}>
        {title}
      </Heading>
    </GridItem>
  );
}
