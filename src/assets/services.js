/*
  MIT License

  Copyright (c) 2022 Open Source  UOM

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  Made by Open Source UoM (https://opensource.uom.gr)

  Project members:
    -Apostolidis
    -Davios
    -Iosifidis
    -Konstantinidis
    -Mpakalis
    -Nasis
    -Omiliades
    -Patsouras
    -Fakidis

*/

export const servicesData = [
  {
    title: "Τμήμα Διασύνδεσης, Διαμεσολάβησης & Καινοτομίας",
    url: "https://clio.uniwa.gr/",
    category: "Γραφείο",
    imgUrl: "https://clio.uniwa.gr/wp-content/uploads/sites/90/2022/06/clio.jpg",
  },
  {
    title: "Τμήμα Πρακτικής",
    url: "https://praktiki.uniwa.gr/",
    category: "Γραφείο",
    imgUrl:
      "https://www.uom.gr/assets/site/public/nodes/8621/7799-praktikiaskisikyriafoto-4.jpg",
  },
  // {
  //   title: "Γραφείο Αποφοίτων",
  //   url: "https://www.uom.gr/apofitoi",
  //   category: "Γραφείο",
  //   imgUrl: "https://www.uom.gr/assets/site/content/alumni/ALUMNI_LOGO_GR.PNG",
  // },
  {
    title: "Erasmus",
    url: "https://erasmus.uniwa.gr/",
    category: "Γραφείο",
    imgUrl: "https://www.uom.gr/assets/site/public/nodes/4228/2644-erasmus-header-2.jpg",
  },
  {
    title: "Τμήμα Αθλητισμού",
    url: "https://sports.uniwa.gr/",
    category: "Γραφείο",
    imgUrl: "https://www.uniwa.gr/wp-content/uploads/2018/11/logo-pada.png",
  },
  {
    title: "Τμήμα Συμβουλευτικής Σταδιοδρομίας & Προσανατολισμού",
    url: "https://stadiodromia.uniwa.gr/",
    category: "Γραφείο",
    imgUrl: "https://www.uniwa.gr/wp-content/uploads/2018/11/logo-pada.png",
  },

  {
    title: "Σύλλογος Αποφοίτων",
    url: "https://clio.uniwa.gr/syllogos-apofoiton/",
    category: "Σύλλογος",
    imgUrl: "https://clio.uniwa.gr/wp-content/uploads/sites/90/2022/06/clio.jpg",
  },

  {
    title: "Συνήγορος του Φοιτητή",
    url: "https://advedu.uniwa.gr/",
    category: "Υπόλοιπες",
    imgUrl:
      "https://foititisonline.gr/wp-content/uploads/2018/08/sinigoros-tou-foititi-3.jpg",
  },
  {
    title: "Επιτροπή Ισότητας Φύλων",
    url: "https://eifpada.uniwa.gr/",
    category: "Υπόλοιπες",
    imgUrl: "https://eifpada.uniwa.gr/wp-content/uploads/sites/577/2022/12/eiflogo-new.png",
  },
  {
    title: "Ψυχοκοινωνική Υποστήριξη",
    url: "https://prosvasi.uniwa.gr/atomiki-psychologiki-ypostirixi/",
    category: "Υπόλοιπες",
    imgUrl: "https://www.uniwa.gr/wp-content/uploads/2018/11/icon.png",
  },
  {
    title: "Υποστήριξη Παρεμβάσεων Κοινωνικής Μέριμνας Φοιτητών ΠΑΔΑ",
    url: "https://prosvasi.uniwa.gr/",
    category: "Υπόλοιπες",
    imgUrl: "https://www.uniwa.gr/wp-content/uploads/2018/11/icon.png",
  },
  {
    title: "Θεατρική Ομάδα",
    url: "https://tritokoudouni.com/",
    category: "Υπόλοιπες",
    imgUrl: "https://www.uniwa.gr/wp-content/uploads/2018/11/icon.png",
  },
  {
    title: "Επιτροπή Ηθικής και Δεοντολογίας της Έρευνας",
    url: "https://research-ethics-comittee.uniwa.gr/",
    category: "Υπόλοιπες",
    imgUrl: "https://research-ethics-comittee.uniwa.gr/wp-content/uploads/sites/267/2020/11/logo_ehde.jpg",
  },
];
