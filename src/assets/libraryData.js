const libraryData = [
  {
    name: "Βιβλιοθήκη Άλσους Αιγάλεω",
    hours: {
      monday_thursday: "9:00 - 19:00",
      friday: "9:00 - 15:30",
      weekend: "Κλειστά",
    },
    phone: ["2105385134", "2105385711"],
  },
  {
    name: "Βιβλιοθήκη Αρχαίου Ελαιώνα",
    hours: {
      monday_thursday: "8.30 - 19.00",
      friday: "8.30 - 16.00",
      weekend: "Κλειστά",
    },
    phone: ["2105381007", "2105381156", "2105381035", "2105381036"],
  },
  {
    name: "Βιβλιοθήκη Αθηνών",
    hours: {
      monday_thursday: "9:00 - 16:00",
      friday: "9:00 - 15:00",
      weekend: "Κλειστά",
    },
    phone: ["2132010092", "2132010092"],
  },
];

export default libraryData;
